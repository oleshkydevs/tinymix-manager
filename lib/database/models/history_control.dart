import 'package:flutter/cupertino.dart';
import 'package:tinymix_manager/models/control.dart';

class HistoryControl {
  String date;
  int ctl;
  String name;
  ControlType type;
  String oldValue;
  String newValue;

  String dateReadable;
  String oldValueAsString;
  String newValueAsString;

  HistoryControl({
    @required this.date,
    @required this.ctl,
    @required this.name,
    @required this.type,
    @required this.oldValue,
    @required this.newValue,
  });

  HistoryControl.fromMap(Map<String, dynamic> map) {
    date = map['date'];
    ctl = map['ctl'];
    name = map['name'];
    type = ControlType.fromString(map['type']);
    oldValue = map['old_value'];
    newValue = map['new_value'];

    final dateTime = DateTime.parse(date);
    final day = dateTime.day.zeroFilled;
    final month = dateTime.month.zeroFilled;
    final hour = dateTime.hour.zeroFilled;
    final minute = dateTime.minute.zeroFilled;
    dateReadable = '$day.$month.${dateTime.year} '
        '$hour:$minute';

    if (type == ControlType.BOOL) {
      oldValueAsString = oldValue == '1' ? 'On' : 'Off';
      newValueAsString = newValue == '1' ? 'On' : 'Off';
    } else {
      oldValueAsString = oldValue;
      newValueAsString = newValue;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'date': date,
      'ctl': ctl,
      'name': name,
      'type': type.name,
      'old_value': oldValue,
      'new_value': newValue,
    };
  }
}

extension on int {
  String get zeroFilled {
    if (this < 10) {
      return '0$this';
    }

    return this.toString();
  }
}
