import 'package:sqflite/sqflite.dart';
import 'package:tinymix_manager/database/models/history_control.dart';

class DatabaseHelper {
  static final DatabaseHelper instance = DatabaseHelper._();

  DatabaseHelper._();

  Database database;

  Future<void> initDatabase() async {
    final databasesPath = await getDatabasesPath();
    final databasePath = '$databasesPath/database.db';

    database = await openDatabase(databasePath, version: 1,
        onCreate: (database, version) async {
      database.transaction((transaction) {
        transaction.execute(
          'CREATE TABLE control_initial_value '
          '(ctl INTEGER PRIMARY KEY, value TEXT)',
        );
        transaction.execute(
          'CREATE TABLE favorite '
          '(ctl INTEGER PRIMARY KEY)',
        );
        transaction.execute(
          'CREATE TABLE history_control '
          '(date TEXT PRIMARY KEY, ctl INTEGER, name TEXT, type TEXT, old_value TEXT, new_value TEXT)',
        );
        return;
      });
    });
  }

  Future<int> insertControlInitialValue(int ctl, String value) {
    return database.rawInsert(
      'INSERT INTO control_initial_value '
      '(ctl, value) VALUES ($ctl, \'$value\')',
    );
  }

  Future<String> getControlInitialValue(int ctl) async {
    final result = await database
        .rawQuery('SELECT * FROM control_initial_value WHERE ctl=$ctl');
    return result?.first['value'];
  }

  Future<Map<int, String>> getAllControlInitialValue() async {
    final result =
        await database.rawQuery('SELECT * FROM control_initial_value');

    Map<int, String> map = {};
    for (final row in result) {
      map[row['ctl']] = row['value'];
    }

    return map;
  }

  Future<int> deleteControlInitialValue(int ctl) {
    return database
        .delete('control_initial_value', where: 'ctl = ?', whereArgs: [ctl]);
  }

  Future<int> deleteAllControlInitialValue() {
    return database.rawDelete('DELETE FROM control_initial_value');
  }

  Future<int> insertFavorite(int ctl) {
    return database.rawInsert(
      'INSERT INTO favorite '
      '(ctl) VALUES ($ctl)',
    );
  }

  Future<int> deleteFavorite(int ctl) {
    return database.delete('favorite', where: 'ctl = ?', whereArgs: [ctl]);
  }

  Future<List<int>> getAllFavorites() async {
    final result = await database.rawQuery('SELECT * FROM favorite');

    List<int> favoritesCtl = [];
    for (final row in result) {
      favoritesCtl.add(row['ctl']);
    }

    return favoritesCtl;
  }

  Future<int> insertHistoryControl(HistoryControl historyControl) {
    return database.insert('history_control', historyControl.toMap());
  }

  Future<List<HistoryControl>> getAllHistoryControl() async {
    final result = await database.query('history_control');
    return result.reversed.map((row) => HistoryControl.fromMap(row)).toList();
  }

  Future<int> deleteHistoryControl(HistoryControl historyControl) {
    return database.delete('history_control',
        where: 'date = ?', whereArgs: [historyControl.date]);
  }

  Future<int> deleteAllHistoryControl() {
    return database.rawDelete('DELETE FROM history_control');
  }
}
