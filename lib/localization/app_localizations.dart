import 'package:flutter/material.dart';
import 'package:tinymix_manager/models/control_details.dart';

class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  ControlDetails getControlDetails(String controlName) {
    return ControlDetails.details[locale.languageCode][controlName];
  }

  String get all {
    return _localizedValues[locale.languageCode]['all'];
  }

  String get favorites {
    return _localizedValues[locale.languageCode]['favorites'];
  }

  String get history {
    return _localizedValues[locale.languageCode]['history'];
  }

  String get allChangesSuccessfullyReverted {
    return _localizedValues[locale.languageCode]
        ['all_changes_successfully_reverted'];
  }

  String get info {
    return _localizedValues[locale.languageCode]['info'];
  }

  String get undoAllChanges {
    return _localizedValues[locale.languageCode]['undo_all_changes'];
  }

  String get clearHistory {
    return _localizedValues[locale.languageCode]['clear_history'];
  }

  String get sureUndoAllControls {
    return _localizedValues[locale.languageCode]['sure_undo_all_controls'];
  }

  String get cancelCapital {
    return _localizedValues[locale.languageCode]['cancel_capital'];
  }

  String get undoCapital {
    return _localizedValues[locale.languageCode]['undo_capital'];
  }

  String get mixerName {
    return _localizedValues[locale.languageCode]['mixer_name'];
  }

  String get numberOfControls {
    return _localizedValues[locale.languageCode]['number_of_controls'];
  }

  String get okCapital {
    return _localizedValues[locale.languageCode]['ok_capital'];
  }

  String get deleteBottomSheetAction {
    return _localizedValues[locale.languageCode]['delete_bottom_sheet_action'];
  }

  String get removeFromFavorites {
    return _localizedValues[locale.languageCode]['remove_from_favorites'];
  }

  String get addToFavorites {
    return _localizedValues[locale.languageCode]['add_to_favorites'];
  }

  String get shareTerminalCommand {
    return _localizedValues[locale.languageCode]['share_terminal_command'];
  }

  String get undoChanges {
    return _localizedValues[locale.languageCode]['undo_changes'];
  }

  String get changesReverted {
    return _localizedValues[locale.languageCode]['changes_reverted'];
  }

  String get removedFromFavorites {
    return _localizedValues[locale.languageCode]['removed_from_favorites'];
  }

  String get addedToFavorites {
    return _localizedValues[locale.languageCode]['added_to_favorites'];
  }

  String get changed {
    return _localizedValues[locale.languageCode]['changed'];
  }

  String get notChanged {
    return _localizedValues[locale.languageCode]['not_changed'];
  }

  String get typeControlName {
    return _localizedValues[locale.languageCode]['type_control_name'];
  }

  String get noRoot {
    return _localizedValues[locale.languageCode]['no_root'];
  }

  String get noFavorites {
    return _localizedValues[locale.languageCode]['no_favorites'];
  }

  String get noHistory {
    return _localizedValues[locale.languageCode]['no_history'];
  }

  String get settings {
    return _localizedValues[locale.languageCode]['settings'];
  }

  String get showControlNumber {
    return _localizedValues[locale.languageCode]['show_control_number'];
  }

  String get scrollbarLabelOff {
    return _localizedValues[locale.languageCode]['scrollbar_label_off'];
  }

  String get scrollbarLabelOn {
    return _localizedValues[locale.languageCode]['scrollbar_label_on'];
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'all': 'All',
      'favorites': 'Favorites',
      'history': 'History',
      'all_changes_successfully_reverted': 'All changes successfully reverted',
      'info': 'Info',
      'undo_all_changes': 'Undo all changes',
      'clear_history': 'Clear History',
      'sure_undo_all_controls':
          'Are you sure undo all controls value to initial state?',
      'cancel_capital': 'CANCEL',
      'undo_capital': 'UNDO',
      'mixer_name': 'Mixer name',
      'number_of_controls': 'Number of controls',
      'ok_capital': 'OK',
      'delete_bottom_sheet_action': 'Delete',
      'remove_from_favorites': 'Remove from favorites',
      'add_to_favorites': 'Add to favorites',
      'share_terminal_command': 'Share terminal command',
      'undo_changes': 'Undo changes',
      'changes_reverted': 'changes reverted',
      'removed_from_favorites': 'removed from favorites',
      'added_to_favorites': 'added to favorites',
      'changed': 'changed',
      'not_changed': 'not changed',
      'type_control_name': 'Type control name',
      'no_root': 'Superuser permission required',
      'no_favorites': 'No favorites',
      'no_history': 'No history',
      'settings': 'Settings',
      'show_control_number': 'Show control numbers in list',
      'scrollbar_label_off': 'Scrollbar doesn\'t contain control number',
      'scrollbar_label_on': 'Scrollbar contains control number',
    },
    'ru': {
      'all': 'Все',
      'favorites': 'Избранные',
      'history': 'История',
      'all_changes_successfully_reverted': 'Все изменения успешно удалены',
      'info': 'Информация',
      'undo_all_changes': 'Удалить все изменения',
      'clear_history': 'Очистить историю',
      'sure_undo_all_controls':
          'Вы уверены, что хотите откатить все значения в начальное состояние?',
      'cancel_capital': 'ОТМЕНА',
      'undo_capital': 'ОТКАТИТЬ',
      'mixer_name': 'Название миксера',
      'number_of_controls': 'Количество элементов управления',
      'ok_capital': 'ОК',
      'delete_bottom_sheet_action': 'Удалить',
      'remove_from_favorites': 'Удалить из избранного',
      'add_to_favorites': 'Добавить в избранное',
      'share_terminal_command': 'Поделиться командой терминала',
      'undo_changes': 'Откатить изменения',
      'changes_reverted': 'изменения удалены',
      'removed_from_favorites': 'удалено из избранного',
      'added_to_favorites': 'добавлено в избранное',
      'changed': 'изменено',
      'not_changed': 'не изменено',
      'type_control_name': 'Введите название эл. упр.',
      'no_root': 'Необходимы права суперпользователя',
      'no_favorites': 'Нет избранных',
      'no_history': 'Нет истории',
      'settings': 'Настройки',
      'show_control_number': 'Показывать номера элементов управления в списке',
      'scrollbar_label_off':
          'Полоса прокрутки не содержит номер элемента управления',
      'scrollbar_label_on':
          'Полоса прокрутки содержит номер элемента управления',
    },
  };

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }
}
