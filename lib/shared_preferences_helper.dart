import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  static SharedPreferencesHelper instance = SharedPreferencesHelper._();

  SharedPreferencesHelper._();

  SharedPreferences preferences;

  Future<void> initPreferences() async {
    preferences = await SharedPreferences.getInstance();
  }

  static const String _bottomNavigationBarIndexKey =
      'bottomNavigationBarIndexKey';
  static const String _showControlNumberKey = 'showControlNumberKey';

  int getBottomNavigationBarIndex() {
    return preferences.getInt(_bottomNavigationBarIndexKey) ?? 0;
  }

  void setBottomNavigationBarIndex(int index) {
    preferences.setInt(_bottomNavigationBarIndexKey, index);
  }

  bool getShowControlNumber() {
    return preferences.getBool(_showControlNumberKey) ?? false;
  }

  void setShowControlNumber(bool show) {
    preferences.setBool(_showControlNumberKey, show);
  }
}
