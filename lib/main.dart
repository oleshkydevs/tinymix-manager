import 'package:flutter/material.dart';
import 'package:tinymix_manager/database/database_helper.dart';
import 'package:tinymix_manager/localization/app_localizations_delegate.dart';
import 'package:tinymix_manager/screens/control_list_screen.dart';
import 'package:tinymix_manager/shared_preferences_helper.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DatabaseHelper.instance.initDatabase();
  await SharedPreferencesHelper.instance.initPreferences();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final primarySwatch = Colors.blue;

    return MaterialApp(
      home: ControlListScreen(),
      theme: ThemeData(
        primarySwatch: primarySwatch,
        chipTheme: ChipTheme.of(context).copyWith(
          selectedColor: Colors.white,
          checkmarkColor: primarySwatch,
          labelStyle: TextStyle(color: primarySwatch),
        ),
      ),
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('ru'),
      ],
    );
  }
}
