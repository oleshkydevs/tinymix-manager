import 'package:flutter/material.dart';

class HistoryControlListPlaceholder extends StatelessWidget {
  const HistoryControlListPlaceholder({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                left: 16,
                bottom: 4,
                top: 14.5,
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  gradient: LinearGradient(
                    colors: [
                      Colors.grey,
                      Colors.grey[400],
                    ],
                  ),
                ),
                width: 200,
                height: 16,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 16,
                bottom: 4,
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  gradient: LinearGradient(
                    colors: [
                      Colors.grey[400],
                      Colors.grey[350],
                    ],
                  ),
                ),
                width: 130,
                height: 16,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 16,
                bottom: 14.5,
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  gradient: LinearGradient(
                    colors: [
                      Colors.grey[400],
                      Colors.grey[350],
                    ],
                  ),
                ),
                width: 100,
                height: 16,
              ),
            ),
            const Divider(height: 0),
          ],
        );
      },
    );
  }
}
