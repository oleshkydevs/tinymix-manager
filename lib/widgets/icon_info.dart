import 'package:flutter/material.dart';

class IconInfo extends StatelessWidget {
  final String info;
  final IconData iconData;

  const IconInfo({
    Key key,
    @required this.info,
    @required this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            iconData,
            size: 42,
          ),
          Container(height: 8),
          Text(
            info,
            style: TextStyle(fontSize: 31),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
