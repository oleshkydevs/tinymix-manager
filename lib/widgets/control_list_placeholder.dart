import 'package:flutter/material.dart';

class ControlListPlaceholder extends StatelessWidget {
  const ControlListPlaceholder({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                left: 16,
                bottom: 8,
                top: 16,
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  gradient: LinearGradient(
                    colors: [
                      Colors.grey,
                      Colors.grey[400],
                    ],
                  ),
                ),
                width: 200,
                height: 16,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 16,
                bottom: 16,
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  gradient: LinearGradient(
                    colors: [
                      Colors.grey[400],
                      Colors.grey[350],
                    ],
                  ),
                ),
                width: 130,
                height: 16,
              ),
            ),
            const Divider(height: 0),
          ],
        );
      },
    );
  }
}
