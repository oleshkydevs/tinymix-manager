import 'package:flutter/material.dart';

class ControlDetails {
  //короткое описание
  final String shortDescription;

  //полное описание
  final String description;

  //описание каждого значения, если перечисление, или переключатель, оставить null если число
  final Map<String, String> valueDescription;

  //рекомендуемое значение по личному опыту к желаемому результату
  final Map<String, String> recommendValues;

  //---------возможные проблемы----------

  //true - да, false - нет
  //может испортить слух
  final bool canHurtEar;

  //может увеличить потребление энергии
  final bool canIncreaseBatteryConsumption;

  //может вывести из строя устройство(динамик, наушники)
  final bool canHurtDevice;

  const ControlDetails._({
    @required this.shortDescription,
    @required this.description,
    @required this.valueDescription,
    @required this.recommendValues,
    @required this.canHurtEar,
    @required this.canIncreaseBatteryConsumption,
    @required this.canHurtDevice,
  });

  static const Map<String, Map<String, ControlDetails>> details = {
    //пример
    'ru': {
      'TTY Mode': const ControlDetails._(
        shortDescription: 'Режим телетайпа',
        description:
            'TTY позволяет совершать звонки, при этом к нему необходимо подключить специальный адаптер.',
        valueDescription: {
          'OFF': 'Выключено',
          'HCO': 'Телетайп с возможностью ...',
          'VCO': 'Телетайп с возможностью ...',
          'FULL': 'Полнофункциональный телетайп',
        },
        recommendValues: {
          '***желаемый результат***': '***значение***',
        },
        canHurtEar: false,
        canIncreaseBatteryConsumption: false, //не знаю
        canHurtDevice: false,
      ),
      'RX1 Digital Volume': const ControlDetails._(
        shortDescription: 'Левый выход наушников',
        description:
            'Осушествляет вывод звука наушников по левому каналу с громкостью в соответствии числовому значению.',
        valueDescription: null,
        recommendValues: {
          'Без негативных последствий': 'Не выше 80',
          '***пример чтобы вы поняли**** хочу оглохнуть': 'Больше 100',
        },
        canHurtEar: true,
        canIncreaseBatteryConsumption: false,
        canHurtDevice: true,
      ),
    },
    'en': {
      //я потом сам переведу
    },
  };
}
