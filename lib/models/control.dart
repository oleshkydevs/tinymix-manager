import 'dart:collection';

class Control {
  int _ctl;
  ControlType _type;
  int _num;
  String _name;

  List _value = [];
  String _valueAsString;
  String _valueAsCommandArgument;

  int get ctl => _ctl;
  ControlType get type => _type;
  int get num => _num;
  String get name => _name;

  List get value => UnmodifiableListView(_value);
  String get valueAsString => _valueAsString;
  String get valueAsCommandArgument => _valueAsCommandArgument;

  Control.fromString(String row) {
    var splitedColumns = row.split(' ').where((str) {
      return str != ' ' && str.isNotEmpty;
    }).toList();

    final first3ColumnsAndPartName = splitedColumns.removeAt(0).split('\t');

    _ctl = int.parse(first3ColumnsAndPartName[0]);
    _type = ControlType.fromString(first3ColumnsAndPartName[1]);

    if (!ControlType.supportedTypes.contains(_type)) {
      _num = 0;
      _valueAsString = 'Unsuported type';
      _valueAsCommandArgument = 'Unsuported type';
      _name = first3ColumnsAndPartName[3];
      return;
    }

    _num = int.parse(first3ColumnsAndPartName[2]);

    for (var i = 0; i < _num; i++) {
      _value.insert(0, _getValue(splitedColumns.removeLast()));
    }

    if (_type == ControlType.BOOL) {
      _valueAsString = _value.map((boolValue) {
        return boolValue ? 'On' : 'Off';
      }).join(' ');
      _valueAsCommandArgument = _value.first ? '1' : '0';
    } else {
      _valueAsString = _value.join(' ');
      _valueAsCommandArgument = _valueAsString;
    }

    _name = '${first3ColumnsAndPartName[3]} ${splitedColumns.join(' ')}'.trim();
  }

  _getValue(String column) {
    switch (_type.type) {
      case int:
        return int.parse(column);
      case String:
        return column;
      case bool:
        return column == 'On' ? true : false;
      default:
        throw Exception();
    }
  }
}

class ControlType {
  final String name;
  final Type type;

  static fromString(String name) {
    switch (name) {
      case 'INT':
        return INT;
      case 'ENUM':
        return ENUM;
      case 'BOOL':
        return BOOL;
      case 'BYTE':
        return BYTE;
      case 'IEC958':
        return IEC958;
      case 'INT64':
        return INT64;
      default:
        return Unknown;
    }
  }

  static const List<ControlType> values = [
    ControlType.INT,
    ControlType.ENUM,
    ControlType.BOOL,
    ControlType.BYTE,
    ControlType.IEC958,
    ControlType.INT64,
    ControlType.Unknown,
  ];

  static const List<ControlType> supportedTypes = [
    ControlType.INT,
    ControlType.ENUM,
    ControlType.BOOL,
    ControlType.Unknown,
  ];

  const ControlType._(this.name, this.type);

  static const ControlType INT = const ControlType._('INT', int);
  static const ControlType ENUM = const ControlType._('ENUM', String);
  static const ControlType BOOL = const ControlType._('BOOL', bool);
  static const ControlType BYTE = const ControlType._('BYTE', String);
  static const ControlType IEC958 = const ControlType._('IEC958', String);
  static const ControlType INT64 = const ControlType._('INT64', int);
  static const ControlType Unknown = const ControlType._('Unknown', String);
}
