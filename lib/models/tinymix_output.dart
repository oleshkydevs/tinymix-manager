import 'dart:collection';

import 'package:tinymix_manager/models/control.dart';

class TinymixOutput {
  String _mixerName;
  List<Control> _controls = [];

  String get mixerName => _mixerName;
  List<Control> get controls => UnmodifiableListView(_controls);

  TinymixOutput(List<String> rows) {
    final mixerNameRow = rows[0];
    final indexStartMixerName = mixerNameRow.indexOf("'") + 1;
    final indexEndMixerName = mixerNameRow.lastIndexOf("'");
    _mixerName = mixerNameRow.substring(indexStartMixerName, indexEndMixerName);

    for (var i = 4; i < rows.length; i++) {
      _controls.add(Control.fromString(rows[i]));
    }
  }
}
