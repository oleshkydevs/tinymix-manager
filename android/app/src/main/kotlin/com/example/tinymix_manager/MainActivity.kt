package com.example.tinymix_manager

import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import java.io.DataOutputStream
import java.lang.Exception

class MainActivity : FlutterActivity() {
    private val CHANNEL = "tinymix"

    private lateinit var suProcess: Process
    private lateinit var outputStream: DataOutputStream

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { methodCall, result ->
            when (methodCall.method) {
                "getSu" -> result.success(getSu())
                "getTinymixOutput" -> {
                    val tinymixOutput = sudo("tinymix")
                    if (tinymixOutput == null) {
                        result.error("error", null, null)
                    } else {
                        result.success(tinymixOutput)
                    }
                }
                "getControl" -> {
                    val controlName = methodCall.arguments<String>()
                    val controlOutput = sudo("tinymix \"$controlName\"")
                    if (controlOutput == null) {
                        result.error("error", null, null)
                    } else {
                        result.success(controlOutput.first())
                    }
                }
                "getControlValue" -> {
                    val controlName = methodCall.arguments<String>()
                    val valueOutput = sudo("tinymix -v \"$controlName\"")
                    if (valueOutput == null) {
                        result.error("error", null, null)
                    } else {
                        result.success(valueOutput.first())
                    }
                }
                "setControl" -> {
                    val (controlName, value) = methodCall.arguments<List<String>>()
                    val output = sudo("tinymix \"$controlName\" \"$value\"")
                    if (output == null) {
                        result.error("error", null, null)
                    } else {
                        result.success(null)
                    }
                }
                else -> result.notImplemented()
            }
        }
    }

    private fun getSu(): Boolean {
        return try {
            suProcess = Runtime.getRuntime().exec("su")
            outputStream = DataOutputStream(suProcess.outputStream)
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    private fun sudo(command: String): List<String>? {
        return try {

            outputStream.writeBytes("$command\n")
            outputStream.flush()

            outputStream.writeBytes("echo ---EOF---\n")
            outputStream.flush()

            val reader = suProcess.inputStream.bufferedReader()
            val result = mutableListOf<String>()
            while (true) {
                val line = reader.readLine()
                if (line == "---EOF---") break
                result += line
            }

            result
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    private fun exitTerminal() {
        try {
            outputStream.writeBytes("exit\n")
            outputStream.flush()

            suProcess.waitFor()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            outputStream.close()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        exitTerminal()
    }
}
